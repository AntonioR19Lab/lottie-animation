# lottie-animation

Ionic/Angular component to use Lottie's animations

## Use

Install ng-lottie

```
npm install --save ng-lottie
```

Import in your components.module.ts

```
// components.module.ts
...
import { LottieAnimationViewModule } from 'ng-lottie';
import { LottieAnimationComponent } from './lottie-animation/lottie-animation.component';

@NgModule({
    imports: [
        ...
        LottieAnimationViewModule.forRoot()
        ],
    declarations: [
        ...
        LottieAnimationComponent
        ],
    exports: [
        ...
        LottieAnimationComponent
        ]
})

...

```

Include your components.module.ts in page module where you want use the animation.

```
// page.module.ts
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SlidesPageRoutingModule,
    ComponentsModule
  ],
  declarations: [SlidesPage]
})

```

in page html where you want use the animation include this tag

```
<lottie-animation [animationConfig]="animationConfig" [animWidth]="animWidth" [animHeight]="animHeight"></lottie-animation>
```

### Attributes

|       Attribute       |       Type       |          Scope          |
|-----------------------|:----------------:|------------------------:|
|   animationConfig     |  AnimationObject | Animation Configuration |
|       animWidth       |       number     |       Animation's width |
|       animHeight      |       number     |      Animation's height |


### Animation Configuration example

|       Attribute       |          Type          |
|-----------------------|:----------------------:|
| path                  | 'assets/animation.json'|
| renderer              | 'canvas'               |
| autoplay              | true                   |
| loop                  | true                   |

For more detail visit ng-module [original page](https://www.npmjs.com/package/ng-lottie).

### Animation download

You'll can download every animation to want from [Lottie Animation site](https://lottiefiles.com/recent). Likely site will ask you to sign in before download

### Events

You can get anim instance using event setedAnim
   ```<lottie-animation (setedAnim)="yourmethod($event)"></lottie-animation>```

the event returns `this` class's instance, and so you can start, stop and else the animation using
   
   * - <instance>.getAnim().stop()
   * - <instance>.getAnim().pause()
   * - and so on
  
### Developers
Name: Antonio Riccio
Email: riccio.antonio29@gmail.com
