import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'lottie-animation',
  templateUrl: './lottie-animation.component.html',
  styleUrls: ['./lottie-animation.component.scss'],
})
export class LottieAnimationComponent {

  private anim: any;
  @Input() animationConfig: any;
  @Input() animWidth: any;
  @Input() animHeight: any;
  @Output() setedAnim = new EventEmitter();

  constructor(
    protected platform: Platform,
  ) {
  }

  public handleAnimation(anim: any) {
    this.anim = anim;
    this.setedAnim.emit(this);
  }

  /**
   * Method to stop animation
   * @return void
   */
  public stop() {
    this.anim.stop();
  }

  /**
   * Method to paly animation
   * @return void
   */
  public play() {
    this.anim.play();
  }

  /**
   * Method to pause animation
   * @return void
   */
  public pause() {
    this.anim.pause();
  }

  /**
   * Method to set speed animation
   * @param speed: number
   * @return void
   */
  public setSpeed(speed: number) {
    this.anim.setSpeed(speed);
  }

  /**
   * Method to get anim
   */
  public getAnim() {
    return this.anim;
  }

}
